package ru.pae.obfuscator;

import java.util.Scanner;

public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите путь к исходному файлу: ");
        String path = scanner.nextLine();
        System.out.println("Введите путь для сохранения результата: ");
        String newPath = scanner.nextLine();
        Obfuscator obfuscator = new Obfuscator(path, newPath);
        obfuscator.obfuscate();
    }
}
